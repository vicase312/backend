const express = require('express');
const request = require('supertest');

const { User } = require('../../../../app/models');
const { EmailNotRegisteredError, TokenInvalidError } = require('../../../../app/errors');

const router = require('../../../../app/router');
const UserRepository = require('../../../../app/repository/userRepository');
const app = express();

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Email confirm test', () => {
  let user = null;
  const userCredential = {
    name: 'arufin',
    email: 'arufin8@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/register')
      .send(userCredential)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  beforeEach(async () => {
    const getUser = await User.findOne({
      where: {
        email: userCredential.email
      }
    });
    user = getUser.dataValues;
  });

  afterEach(async () => {
    await User.update(
      {
        status: 1
      },
      { where: { email: userCredential.email } }
    );

    const getUser = await User.findOne({
      where: {
        email: userCredential.email
      }
    });

    user = getUser.dataValues;
  });

  afterAll(async () => {
    await User.destroy({
      where: {
        email: userCredential.email
      }
    });
  });

  it('Failed confirm: Token Invalid', (done) => {
    const userCredentialFail = {
      email: userCredential.email,
      token: 'salah',
      newPassword: '123'
    };
    const expectedError = new TokenInvalidError();
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/password/reset')
      .send(userCredentialFail)
      .expect(401)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });

  it('Resend reset password to user email', (done) => {
    const payload = {
      email: userCredential.email,
      token: user.token,
      newPassword: '123'
    };
    request(app)
      .post('/v1/auth/password/reset')
      .send(payload)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  it('Failed resend reset password: email not registered', (done) => {
    const userConfirmationPayload = {
      email: 'notregister@gmail.com'
    };
    const expectedError = new EmailNotRegisteredError(userConfirmationPayload.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/password/reset')
      .send(userConfirmationPayload)
      .expect(404)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });
});
