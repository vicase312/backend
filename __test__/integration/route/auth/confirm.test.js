const express = require('express');
const request = require('supertest');

const { User } = require('../../../../app/models');
const {
  EmailNotRegisteredError,
  TokenInvalidError,
  UserAlreadyActiveError
} = require('../../../../app/errors');

const router = require('../../../../app/router');
const UserRepository = require('../../../../app/repository/userRepository');
const app = express();

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Email confirm test', () => {
  let user = null;
  const userCredential = {
    name: 'arufin',
    email: 'arufin@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/register')
      .send(userCredential)
      .expect(201)
      .then(async (res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  beforeEach(async () => {
    const getUser = await UserRepository.findByEmail(userCredential.email);
    user = getUser.dataValues;
  });

  afterAll(async () => {
    await User.destroy({
      where: {
        email: userCredential.email
      }
    });
  });

  it('Failed confirm: Token Invalid', (done) => {
    const userCredentialFail = {
      email: user.email,
      token: 'salah'
    };
    const expectedError = new TokenInvalidError();
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/confirm')
      .send(userCredentialFail)
      .expect(400)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });

  it('Failed confirm: email not registered', (done) => {
    const userConfirmationPayload = {
      email: 'notregister@gmail.com',
      token: '123456'
    };
    const expectedError = new EmailNotRegisteredError(userConfirmationPayload.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/confirm')
      .send(userConfirmationPayload)
      .expect(404)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });

  it('Failed confirm:User already active', (done) => {
    const userConfirmationPayload = {
      email: 'rayen@mailinator.com',
      token: '1234567'
    };
    const expectedError = new UserAlreadyActiveError(userConfirmationPayload.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/confirm')
      .send(userConfirmationPayload)
      .expect(400)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });

  it('Success confirm', (done) => {
    const userConfirmationPayload = {
      email: user.email,
      token: user.token
    };
    request(app)
      .post('/v1/auth/confirm')
      .send(userConfirmationPayload)
      .expect(201)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
