const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { User } = require('../../../../app/models');
const { encryptPassword } = require('../../../../app/libs/password');
const { EmailNotRegisteredError, WrongPasswordError } = require('../../../../app/errors');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Update Password test', () => {
  afterAll(async () => {
    await User.update(
      {
        password: encryptPassword('1234567')
      },
      {
        where: {
          name: 'Al',
          email: 'al@mailinator.com'
        }
      }
    );
  });
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  const payload = {
    oldPassword: '1234567',
    newPassword: 'algifari123'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('Success update password', (done) => {
    request(app)
      .put('/v1/auth/password')
      .set('Authorization', `Bearer ${token}`)
      .send(payload)
      .expect(202)
      .then((res) => {
        expect(res.body.success).toEqual(true);
        done();
      })
      .catch(done);
  });

  // it('Failed update password: email not registered', (done) => {
  //   const tokenfail =
  //     'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNiwibmFtZSI6IkFsIiwiZW1haWwiOiJub3RyZWdpc3RlckBiaW5hci5jby5pZCJ9LCJpYXQiOjE2NTYzMzYzODh9.4mNLLD6zSq-_8n6snCT-Bz_k88xfOEMcbH4Fbxq8lOE';
  //   const userConfirmationPayload = {
  //     email: 'notregister@gmail.com'
  //   };
  //   const expectedError = new EmailNotRegisteredError(userConfirmationPayload.email);
  //   const expectedResponse = {
  //     error: {
  //       details: expectedError.details,
  //       message: expectedError.message,
  //       name: expectedError.name
  //     }
  //   };
  //   request(app)
  //     .post('/v1/auth/resendEmail')
  //     .send(userConfirmationPayload)
  //     .set('Authorization', `Bearer ${tokenfail}`)
  //     .expect(404)
  //     .then((res) => {
  //       expect(res.body.error.message).toEqual(expectedResponse.error.message);
  //       done();
  //     })
  //     .catch(done);
  // });

  it('Login failed : Password is not correct', (done) => {
    const expectedError = new WrongPasswordError(userCredential.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };

    request(app)
      .put('/v1/auth/password')
      .set('Authorization', `Bearer ${token}`)
      .send(payload)
      .expect(401)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });
});
