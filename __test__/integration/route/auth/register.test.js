const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { EmailAlreadyTakenError } = require('../../../../app/errors');
const { User } = require('../../../../app/models');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Register test', () => {
  afterAll(async () => {
    await User.destroy({
      where: {
        name: 'Ryan',
        email: 'ryan@mailinator.com'
      }
    });
  });

  it('Success register', (done) => {
    const userCredential = {
      name: 'Ryan',
      email: 'ryan@mailinator.com',
      password: '123456'
    };
    request(app)
      .post('/v1/auth/register')
      .send(userCredential)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  it('Register failed : email exists', (done) => {
    const userCredential = {
      name: 'Al Gifari',
      email: 'al@mailinator.com',
      password: '1234567'
    };
    const expectedError = new EmailAlreadyTakenError(userCredential.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };

    request(app)
      .post('/v1/auth/register')
      .send(userCredential)
      .expect(422)
      .then((res) => {
        expect(res.body).toEqual(expectedResponse);
        done();
      })
      .catch(done);
  });
});
