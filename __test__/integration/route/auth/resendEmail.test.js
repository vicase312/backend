const express = require('express');
const request = require('supertest');

const { User } = require('../../../../app/models');
const { EmailNotRegisteredError, UserAlreadyActiveError } = require('../../../../app/errors');

const router = require('../../../../app/router');
const app = express();

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Email confirm test', () => {
  const userCredential = {
    name: 'arufin',
    email: 'arufin6@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/register')
      .send(userCredential)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  afterAll(async () => {
    await User.destroy({
      where: {
        email: userCredential.email
      }
    });
  });

  it('Resend token to user email', (done) => {
    const payload = {
      email: userCredential.email
    };
    request(app)
      .post('/v1/auth/resendEmail')
      .send(payload)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  it('Failed resend token: email not registered', (done) => {
    const userConfirmationPayload = {
      email: 'notregister@gmail.com'
    };
    const expectedError = new EmailNotRegisteredError(userConfirmationPayload.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/resendEmail')
      .send(userConfirmationPayload)
      .expect(404)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });

  it('Failed resend token: User Already Active', (done) => {
    const userConfirmationPayload = {
      email: 'rayen@mailinator.com',
      token: '1234567'
    };
    const expectedError = new UserAlreadyActiveError(userConfirmationPayload.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };
    request(app)
      .post('/v1/auth/resendEmail')
      .send(userConfirmationPayload)
      .expect(400)
      .then((res) => {
        expect(res.body.error.message).toEqual(expectedResponse.error.message);
        done();
      })
      .catch(done);
  });
});
