const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const {
  EmailNotRegisteredError,
  WrongPasswordError,
  UserInactive
} = require('../../../../app/errors');
const { User } = require('../../../../app/models');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Login test', () => {
  const userRegisterCredential = {
    name: 'arufin',
    email: 'arufin7@mailinator.com',
    password: '1234567'
  };

  beforeEach((done) => {
    request(app)
      .post('/v1/auth/register')
      .send(userRegisterCredential)
      .expect(201)
      .then((res) => {
        expect(res.body.success).toBeTruthy();
        done();
      })
      .catch(done);
  });

  it('Success login', (done) => {
    const userCredential = {
      email: 'rayen@mailinator.com',
      password: '1234567'
    };
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        done();
      })
      .catch(done);
  });

  it('Login failed : email not found', (done) => {
    const userCredential = {
      email: 'fikri123@mailinator.com',
      password: '123456'
    };
    const expectedError = new EmailNotRegisteredError(userCredential.email);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };

    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(404)
      .then((res) => {
        expect(res.body).toEqual(expectedResponse);
        done();
      })
      .catch(done);
  });

  it('Login failed : wrong password', (done) => {
    const userCredential = {
      email: 'rayen@mailinator.com',
      password: '123456'
    };
    const expectedError = new WrongPasswordError();
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };

    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(401)
      .then((res) => {
        expect(res.body).toEqual(expectedResponse);
        done();
      })
      .catch(done);
  });

  it('Login failed : user is not active', (done) => {
    const expectedError = new UserInactive(userRegisterCredential);
    const expectedResponse = {
      error: {
        details: expectedError.details,
        message: expectedError.message,
        name: expectedError.name
      }
    };

    request(app)
      .post('/v1/auth/login')
      .send(userRegisterCredential)
      .expect(403)
      .then((res) => {
        expect(res.body).toEqual(expectedResponse);
        done();
      })
      .catch(done);
  });

  afterEach(async () => {
    await User.destroy({
      where: {
        email: 'arufin7@mailinator.com'
      }
    });
  });
});
