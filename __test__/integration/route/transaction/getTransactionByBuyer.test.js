const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('get transaction by buyer', () => {
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('success get transaction', (done) => {
    request(app)
      .get(`/v1/user/transaction/buyer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
