const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('upload image', () => {
  it('success upload image', (done) => {
    request(app)
      .post(`/v1/upload`)
      .set('Accept', 'application/json')
      .attach('image', `${__dirname}/file.png`)
      .field('folder', 'users')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
