const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products } = require('../../../../app/models');
const { decodeToken } = require('../../../../app/libs/token');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('get offer by seller', () => {
  let token = null;
  let product_id = null;
  let dataProduct = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then(async (res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        const data = decodeToken(token);
        const user_id = data.user.id;

        await Products.create({
          name: 'hallo',
          price: 100000,
          user_id: user_id,
          category_id: 1,
          description: 'Motor baru nih',
          images: ['https://google.com']
        });

        dataProduct = await Products.findOne({
          where: {
            user_id
          }
        });
        done();
      })
      .catch(done);
  });

  it('handle check offer status', (done) => {
    product_id = dataProduct.dataValues.id;
    request(app)
      .get(`/v1/product/${product_id}/offer`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
