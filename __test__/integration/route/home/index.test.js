const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Home index', () => {
  it('Success indexing', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
