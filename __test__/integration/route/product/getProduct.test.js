const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
// const database = require('../../../unit/database');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);

describe('Get product', () => {
  // beforeEach(async () => {
  //   return database();
  // });
  it('Success get all product', (done) => {
    request(app)
      .get('/v1/products')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('Success get product by slug', (done) => {
    request(app)
      .get('/v1/product/samsung-j2-prime')
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('Fail get product by slug', (done) => {
    request(app)
      .get('/v1/product/blab-blabla')
      .expect(404)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
