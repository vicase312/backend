const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
// const database = require('../../../unit/database');

jest.setTimeout(20000);
app.use(express.json());
router.apply(app);
const { Products } = require('../../../../app/models');

describe('Create product', () => {
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeAll((done) => {
    // database();
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  afterAll(async () => {
    await Products.destroy({
      where: {
        name: 'Motor Baru'
      }
    });
    await Products.destroy({
      where: {
        name: 'Motor Lama'
      }
    });
  });

  it('Success create draft ', (done) => {
    const productRequest = {
      name: 'Motor Baru',
      price: 100000,
      category_id: 1,
      description: 'Motor baru nih',
      images: ['https://google.com'],
      status: 0
    };

    request(app)
      .post('/v1/products')
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send(productRequest)
      .expect(201)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('Success create published ', (done) => {
    const productRequest = {
      name: 'Motor Lama',
      price: 100000,
      category_id: 1,
      description: 'Motor baru nih',
      images: ['https://google.com'],
      status: 1
    };

    request(app)
      .post('/v1/products')
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send(productRequest)
      .expect(201)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
