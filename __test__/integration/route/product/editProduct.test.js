const express = require('express');
const request = require('supertest');

const app = express();
const router = require('../../../../app/router');
const { Products } = require('../../../../app/models');

jest.setTimeout(30000);
app.use(express.json());
router.apply(app);

describe('delete product', () => {
  let mockData = {
    name: 'new name',
    price: 20000,
    category_id: 1,
    description: 'ini adalah product baru untuk testinggg',
    status: 0,
    images: ['google.com', 'hallohallobandung.com']
  };

  let product_id = null;
  let token = null;
  const userCredential = {
    email: 'al@mailinator.com',
    password: '1234567'
  };

  beforeEach(async () => {
    product_id = await Products.findOne({
      where: {
        slug: 'yamaha-vixion-2014'
      }
    });
  });

  beforeAll((done) => {
    request(app)
      .post('/v1/auth/login')
      .send(userCredential)
      .expect(200)
      .then((res) => {
        expect(res.body.data.accessToken).toBeTruthy();
        token = res.body.data.accessToken;
        done();
      })
      .catch(done);
  });

  it('success update products', (done) => {
    product_id = product_id.dataValues.id;
    request(app)
      .put(`/v1/products/${product_id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send(mockData)
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });

  it('fail update products', (done) => {
    product_id = -1;
    request(app)
      .put(`/v1/products/${product_id}`)
      .set('Authorization', `Bearer ${token}`)
      .set('Accept', 'application/json')
      .send(mockData)
      .expect(200)
      .then((res) => {
        done();
      })
      .catch(done);
  });
});
