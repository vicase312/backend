const NotificationMapper = {
  PUBLISH_PRODUCT: 1,
  SUBMIT_OFFER: 2,
  REJECT_OFFER: 3,
  ACCEPT_OFFER: 4,
  CLOSE_OFFER: 5
};

module.exports = {
  NotificationMapper
};
