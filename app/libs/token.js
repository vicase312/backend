const jwt = require('jsonwebtoken');
const { uuid } = require('uuidv4');

const { JWT_SIGNATURE_KEY } = require('../../config/application');

const createToken = (user) => {
  user.password = undefined;
  return jwt.sign(
    {
      user
    },
    JWT_SIGNATURE_KEY
  );
};

const decodeToken = (token) => {
  return jwt.verify(token, JWT_SIGNATURE_KEY);
};

const getRandomToken = () => {
  return uuid();
};

module.exports = {
  createToken,
  decodeToken,
  getRandomToken
};
