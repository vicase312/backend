const SuccessFetchResponse = (res, data, message) => {
  return res.status(200).json(
    message
      ? {
          success: true,
          message: message,
          data: data
        }
      : {
          success: true,
          data: data
        }
  );
};

const SuccessFetchMetaResponse = (res, data, meta) => {
  return res.status(200).json({
    success: true,
    data: data,
    meta: meta
  });
};

const SuccessCreateResponse = (res, message, data) => {
  return res.status(201).json(
    message
      ? {
          success: true,
          message: message,
          data: data
        }
      : {
          success: true,
          data: data
        }
  );
};
const SuccessUpdateResponse = (res, data, message) => {
  return res.status(202).json(
    message
      ? data
        ? {
            success: true,
            message: message,
            data: data
          }
        : {
            success: true,
            message: message
          }
      : {
          success: true,
          data: data
        }
  );
};

const SuccessDeleteResponse = (res, message, data) => {
  return res.status(202).json(
    message
      ? {
          success: true,
          message: message,
          data: data
        }
      : {
          success: true,
          data: data
        }
  );
};

module.exports = {
  SuccessFetchResponse,
  SuccessCreateResponse,
  SuccessUpdateResponse,
  SuccessDeleteResponse,
  SuccessFetchMetaResponse
};
