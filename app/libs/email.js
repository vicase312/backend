const nodemailer = require('nodemailer'),
  ejs = require('ejs');

var transport = nodemailer.createTransport({
  host: process.env.EMAIL_HOST || 'mail.hellocreative.my.id',
  port: process.env.EMAIL_PORT || '465',
  auth: {
    user: process.env.EMAIL_USERNAME || 'secondhand@hellocreative.my.id',
    pass: process.env.EMAIL_PASSWORD || 'rahasia123'
  }
});

const emailTypeMapper = {
  confirmation: {
    subject: 'Konfirmasi Email - Secondhand',
    template: '/templates/confirmation.ejs'
  },
  password: {
    subject: 'Reset Password - Secondhand',
    template: '/templates/password.ejs'
  }
};

const sendEmail = (receiver, type, content) => {
  const emailType = emailTypeMapper[type];

  ejs.renderFile(__dirname + emailType.template, { name: receiver, content }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      var mailOptions = {
        from: process.env.EMAIL_USERNAME,
        to: receiver,
        subject: emailType.subject,
        html: data
      };

      transport.sendMail(mailOptions, (error, info) => {
        if (error) {
          return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
      });
    }
  });
};

module.exports = {
  sendEmail
};
