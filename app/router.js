const {
  ApplicationController,
  AuthenticationController,
  CategoryController,
  UploadController,
  ProductController,
  OfferController,
  CityController,
  NotificationController,
  TransactionController
} = require('./controllers');
const { checkAccessToken: auth } = require('./middleware/auth');
const uploader = require('./middleware/multer');

function apply(app) {
  const applicationController = new ApplicationController();
  const authenticationController = new AuthenticationController();
  const categoryController = new CategoryController();
  const uploadController = new UploadController();
  const productController = new ProductController();
  const offerController = new OfferController();
  const cityController = new CityController();
  const notificationController = new NotificationController();
  const transactionController = new TransactionController();

  app.get('/', applicationController.handleGetRoot);

  // AUTHENTICATION
  app.post('/v1/auth/register', authenticationController.handleRegister);
  app.post('/v1/auth/login', authenticationController.handleLogin);
  app.post('/v1/auth/confirm', authenticationController.handleConfirm);
  app.post('/v1/auth/resendEmail', authenticationController.handleResendEmail);
  app.post('/v1/auth/password/forget', authenticationController.handleForgetPassword);
  app.post('/v1/auth/password/reset', authenticationController.handleResetPassword);
  app.get('/v1/auth/whoami', auth, authenticationController.handleGetUser);
  app.put('/v1/auth/profile', auth, authenticationController.handleUpdateProfile);
  app.put('/v1/auth/password', auth, authenticationController.handleUpdatePassword);

  // CATEGORY
  app.get('/v1/categories', categoryController.handleListCategory);

  // CITY
  app.get('/v1/cities', cityController.handleListCity);

  // PRODUCT
  app.get('/v1/products', productController.handleListProduct);
  app.post('/v1/products', auth, productController.handleCreateProducts);
  app.put('/v1/products/:id', auth, productController.handleEditProduct);
  app.get('/v1/product/:slug', productController.handleProduct);
  app.get('/v1/user/products', auth, productController.handleProductByOwner);
  app.get('/v1/user/products-count', auth, productController.handleProductCountByOwner);
  app.get('/v1/user/product/:id', auth, productController.handleProductByOwnerAndId);
  app.get('/v1/product/:id/like', auth, productController.handleProductLikeStatus);
  app.post('/v1/product/:id/like', auth, productController.handleProductLikes);
  app.delete('/v1/products/:id/delete', auth, productController.handleDeleteProduct);
  app.get('/v1/user/products-sold', auth, productController.handleProductSoldByOwner);
  app.get('/v1/user/products-liked', auth, productController.handleProductsLiked);
  app.get('/v1/user/products-not-sold', auth, productController.handleProductNotSoldByOwner);

  // OFFER
  app.get('/v1/product/:id/offer', auth, offerController.handleCheckOfferStatus);
  app.post('/v1/product/:id/offer', auth, offerController.handleCreateOffer);
  app.put('/v1/product/:id/offer', auth, offerController.handleUpdateStatus);
  app.put('/v1/product/:id/offer/:oID', auth, offerController.handleUpdateProductStatus);
  app.get('/v1/user/offer/received', auth, offerController.handleListOfferReceived);
  app.get('/v1/user/offer/made', auth, offerController.handleListOfferMade);
  app.get('/v1/user/offer/:id', auth, offerController.handleGetOffer);
  app.get('/v1/product/:id/offer/list', auth, offerController.handleGetOfferByProductID);

  // NOTIFICATION
  app.get('/v1/notifications', auth, notificationController.handleGetNotificationByUserID);
  app.put('/v1/notification/:id', auth, notificationController.handleUpdateRead);

  // UPLOAD
  app.post('/v1/upload', uploader.single('image'), uploadController.upload);

  // TRANSACTION
  app.get('/v1/user/transaction/seller', auth, transactionController.handleListTransactionSeller);
  app.get('/v1/user/transaction/buyer', auth, transactionController.handleListTransactionBuyer);

  return app;
}

module.exports = { apply };
