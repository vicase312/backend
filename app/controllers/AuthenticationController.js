const ApplicationController = require('./ApplicationController');
const UserService = require('../services/userService');
const {
  SuccessFetchResponse,
  SuccessCreateResponse,
  SuccessUpdateResponse
} = require('../libs/response');

class AuthenticationController extends ApplicationController {
  handleLogin = async (req, res, next) => {
    try {
      const reqBody = req.body;
      const accessToken = await UserService.login({ reqBody, response: res });
      return SuccessFetchResponse(res, { accessToken });
    } catch (err) {
      next(err);
    }
  };

  handleRegister = async (req, res, next) => {
    try {
      const reqBody = req.body;
      const accessToken = await UserService.register({ reqBody, response: res });
      return SuccessCreateResponse(res, 'Success register user');
    } catch (err) {
      next(err);
    }
  };

  handleGetUser = async (req, res) => {
    const user = req.user;
    return SuccessFetchResponse(res, user);
  };

  handleUpdateProfile = async (req, res, next) => {
    try {
      const id = req.user.id;
      const newData = req.body;
      const updateUser = await UserService.update({ id, newData }, res);
      return SuccessUpdateResponse(res, null, 'success update data');
    } catch (err) {
      next(err);
    }
  };

  handleUpdatePassword = async (req, res, next) => {
    try {
      const email = req.user.email;
      const newData = req.body;
      const updateUser = await UserService.updatePassword({ email, newData, response: res });
      return SuccessUpdateResponse(res, 'success update password');
    } catch (err) {
      next(err);
    }
  };

  handleConfirm = async (req, res, next) => {
    try {
      const reqBody = req.body;
      const confirmUser = await UserService.confirm({ reqBody, response: res });
      return SuccessCreateResponse(res, 'Email Confirmation Success');
    } catch (err) {
      next(err);
    }
  };

  handleResendEmail = async (req, res, next) => {
    try {
      const reqBody = req.body;
      await UserService.resendEmail({ reqBody, response: res });
      return SuccessCreateResponse(res, 'Resend email confirmation success');
    } catch (err) {
      next(err);
    }
  };

  handleForgetPassword = async (req, res, next) => {
    try {
      const reqBody = req.body;
      await UserService.forgetPassword({ reqBody, response: res });
      return SuccessCreateResponse(res, 'Send email forget password');
    } catch (err) {
      next(err);
    }
  };

  handleResetPassword = async (req, res, next) => {
    try {
      const reqBody = req.body;
      await UserService.resetPassword({ reqBody, response: res });
      return SuccessCreateResponse(res, 'Success reset password');
    } catch (err) {
      next(err);
    }
  };
}

module.exports = AuthenticationController;
