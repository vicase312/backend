const ApplicationController = require('./ApplicationController');
const { SuccessFetchResponse } = require('../libs/response');
const cloudinary = require('../libs/cloudinary');

class UploadController extends ApplicationController {
  upload = async (req, res) => {
    const upload = await cloudinary.v2.uploader.upload(req.file.path, {
      folder: req.body.folder || null
    });

    return SuccessFetchResponse(res, { file: upload.secure_url }, 'Success uploading file');
  };
}

module.exports = UploadController;
