const ApplicationController = require('./ApplicationController');
const CategoryService = require('../services/categoryService');
const { SuccessFetchResponse } = require('../libs/response');

class CategoryController extends ApplicationController {
  handleListCategory = async (req, res) => {
    const category = await CategoryService.list();
    return SuccessFetchResponse(res, category);
  };
}

module.exports = CategoryController;
