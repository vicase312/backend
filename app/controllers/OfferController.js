const ApplicationController = require('./ApplicationController');
const ProductService = require('../services/productService');
const OfferService = require('../services/offerService');
const NotificationService = require('../services/notificationService');
const {
  SuccessCreateResponse,
  SuccessFetchResponse,
  SuccessUpdateResponse
} = require('../libs/response');
const { Offers, Transaction } = require('../models');

class OfferController extends ApplicationController {
  handleCreateOffer = async (req, res) => {
    const id = req.user.id;
    const product_id = req.params.id;
    const reqBody = req.body;

    try {
      const offer = await OfferService.create(id, reqBody, product_id);
      await NotificationService.submitOffer(offer?.id, offer?.user_id, offer?.seller_id);
      return SuccessCreateResponse(res, 'Offer created successfully', offer);
    } catch (e) {
      console.log(e);
    }
  };

  handleCheckOfferStatus = async (req, res) => {
    const user_id = req.user.id;
    const product_id = req.params.id;

    try {
      const status = await OfferService.checkStatus(user_id, product_id);
      return SuccessFetchResponse(res, status);
    } catch (e) {
      console.log(e);
    }
  };

  handleUpdateStatus = async (req, res) => {
    const offer_id = req.params.id;
    const reqBody = req.body;
    const result = await OfferService.update(offer_id, reqBody);

    if (reqBody.status === 0) {
      const offer = await OfferService.getOfferByID(offer_id);
      await NotificationService.rejectOffer(offer_id, offer?.user_id);
    }

    if (reqBody.status === 2) {
      const offer = await OfferService.getOfferByID(offer_id);
      await NotificationService.acceptOffer(offer_id, offer?.user_id);
    }

    return SuccessUpdateResponse(
      res,
      result,
      reqBody.status ? 'Sukses Menerima Offer' : 'Sukses Menolak Offer'
    );
  };

  handleListOfferMade = async (req, res) => {
    const user_id = req.user.id;
    const data = await OfferService.getOfferByUserID(user_id);
    return SuccessFetchResponse(res, data);
  };

  handleListOfferReceived = async (req, res) => {
    const user_id = req.user.id;
    const data = await OfferService.getOfferBySellerID(user_id);
    return SuccessFetchResponse(res, data);
  };

  handleGetOffer = async (req, res) => {
    const id = req.params.id;
    const data = await OfferService.getOfferByID(id);
    return SuccessFetchResponse(res, data);
  };

  handleGetOfferByProductID = async (req, res) => {
    const product_id = req.params.id;
    const data = await OfferService.getOfferByProductID(product_id);
    return SuccessFetchResponse(res, data);
  };

  handleUpdateProductStatus = async (req, res) => {
    const product_id = req.params.id;
    const offer_id = req.params.oID;
    const reqBody = req.body;

    if (reqBody.status === 2) {
      await ProductService.editData(product_id, reqBody);
      await Offers.update(
        {
          status: 3
        },
        {
          where: {
            id: offer_id
          }
        }
      );

      const offer = await OfferService.getOfferByID(offer_id);

      const trx = await Transaction.create({
        buyer_id: offer.user_id,
        buyer_name: offer.user.name,
        buyer_address: offer.user.address,
        buyer_phone: offer.user.phone,
        seller_id: offer.seller_id,
        seller_name: offer.seller.name,
        seller_address: offer.seller.address,
        seller_phone: offer.seller.phone,
        offer_id: offer_id,
        price: offer.offer_price,
        product_id: offer.product_id,
        invoice_id:
          'INV' + new Date().toISOString().replace('-', '').replace(':', '').replace('.', '')
      });
      await NotificationService.productClosed(offer_id);
    } else if (reqBody.status === 0) {
      console.log('offerID:' + offer_id);
      await Offers.update(
        {
          status: reqBody.status
        },
        {
          where: {
            id: offer_id
          }
        }
      );
      const offer = await OfferService.getOfferByID(offer_id);
      await NotificationService.rejectOffer(offer_id, offer?.user_id);
    }

    // const data = await OfferService.getOfferByProductID(product_id);
    return SuccessUpdateResponse(res, 'Success update status penjualan');
  };
}
module.exports = OfferController;
