const CityRepository = require('../repository/cityRepository');

class CityService {
  list = async () => {
    const cities = await CityRepository.findAll();
    return cities;
  };
}

module.exports = new CityService();
