const OfferRepository = require('../repository/offerRepository');
const ProductRepository = require('../repository/productRepository');
const { Offers, Products, User, ProductImage, City } = require('../models');
class OfferService {
  create = async (id, reqBody, product_id) => {
    const product = await ProductRepository.getByProductId(product_id);

    const offer = await OfferRepository.create(id, reqBody, product_id, product.user_id);
    return offer;
  };

  update = async (offer_id, reqBody) => {
    if (reqBody.status === 2) {
      const offer = await Offers.findOne({
        where: {
          id: offer_id
        },
        include: [
          { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } },
          { model: User, as: 'user', include: { model: City, as: 'city' } }
        ]
      });
      await OfferRepository.rejectOffer(offer?.product_id);
    }
    const result = await OfferRepository.update(offer_id, reqBody);
    return result;
  };

  checkStatus = async (user_id, product_id) => {
    const status = await OfferRepository.getOfferStatusByProductUser(user_id, product_id);
    if (status) {
      return 1;
    } else {
      return 0;
    }
  };

  getOfferByID = async (id) => {
    const offer = await Offers.findOne({
      where: {
        id
      },
      include: [
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } },
        { model: User, as: 'user', include: { model: City, as: 'city' } },
        { model: User, as: 'seller', include: { model: City, as: 'city' } }
      ]
    });

    return offer;
  };

  getOfferByProductID = async (product_id) => {
    const offer = await OfferRepository.getOfferByProductID(product_id);
    return offer;
  };

  getOfferBySellerID = async (user_id) => {
    const offer = await OfferRepository.getOfferBySellerID(user_id);
    return offer;
  };

  getOfferByUserID = async (user_id) => {
    const offer = await OfferRepository.getOfferByUserID(user_id);
    return offer;
  };
}

module.exports = new OfferService();
