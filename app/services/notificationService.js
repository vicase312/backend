const { NotificationMapper } = require('../libs/mapper');
const NotificationRepository = require('../repository/notificationRepository');
const ProductImageRepository = require('../repository/productImageRepository');
const OfferRepository = require('../repository/offerRepository');
const ProductRepository = require('../repository/productRepository');
const { RecordNotFoundError } = require('../errors');

class NotificationService {
  getByUserId = async (qs) => {
    const where = {
      user_id: qs.user_id
    };
    const notification = await NotificationRepository.filterPagination({ where }, qs);
    return notification;
  };
  createProduct = async (userID, productID) => {
    let product = null;
    product = await ProductRepository.getByOwnerAndId(userID, productID);

    const image = await ProductImageRepository.findByProduct(productID);

    const newProduct = {
      id: product?.id,
      name: product?.name,
      price: product?.price,
      category: product?.category,
      images: image
    };

    const newData = {
      route: '/user/product/' + product.id + '/edit',
      user_id: userID,
      type: NotificationMapper.PUBLISH_PRODUCT,
      payload: newProduct
    };

    const createNotif = await NotificationRepository.create({ newData });
    return createNotif;
  };

  submitOffer = async (offerID, buyerID, sellerID) => {
    const offer = await OfferRepository.getOfferByID(offerID);

    const newData = {
      route: '/user/offer/' + offer.id,
      user_id: buyerID,
      type: NotificationMapper.SUBMIT_OFFER,
      payload: offer
    };

    await NotificationRepository.create({ newData });

    const newData2 = {
      route: '/user/offer/' + offer.id,
      user_id: sellerID,
      type: NotificationMapper.SUBMIT_OFFER,
      payload: offer
    };

    await NotificationRepository.create({ newData: newData2 });
    return 1;
  };

  rejectOffer = async (offerID, buyerID) => {
    const offer = await OfferRepository.getOfferByID(offerID);

    const newData = {
      route: '/user/offer/' + offer.id,
      user_id: buyerID,
      type: NotificationMapper.REJECT_OFFER,
      payload: offer
    };

    await NotificationRepository.create({ newData });
    return 1;
  };

  acceptOffer = async (offerID, buyerID) => {
    const offer = await OfferRepository.getOfferByID(offerID);

    const newData = {
      route: '/user/offer/' + offer.id,
      user_id: buyerID,
      type: NotificationMapper.ACCEPT_OFFER,
      payload: offer
    };

    await NotificationRepository.create({ newData });
    return 1;
  };

  productClosed = async (offerID) => {
    const offer = await OfferRepository.getOfferByID(offerID);

    const newData = {
      route: '/user/offer/' + offer.id,
      user_id: offer.user_id,
      type: NotificationMapper.CLOSE_OFFER,
      payload: offer
    };

    await NotificationRepository.create({ newData });
    return 1;
  };

  updateRead = async (id, response) => {
    const where = {
      id: id
    };
    const notification = await NotificationRepository.filterOne({ where });
    if (!notification) {
      const err = new RecordNotFoundError(id);
      return response.status(404).json(err);
    }
    const updateRead = await NotificationRepository.updateRead(id);
    return updateRead;
  };

  count = async (qs) => {
    const { user_id } = qs;
    let query = {};
    if (user_id) {
      query = { ...query, user_id: user_id };
    }
    const notification = await NotificationRepository.count({ query: query });
    return notification;
  };
}

module.exports = new NotificationService();
