const TransactionRepository = require('../repository/transactionRepository');

class TransactionService {
  getTransactionBySellerID = async (user_id) => {
    const transactions = await TransactionRepository.getBySellerID(user_id);
    return transactions;
  };

  getTransactionByBuyerID = async (user_id) => {
    const transactions = await TransactionRepository.getByBuyerID(user_id);
    return transactions;
  };
}

module.exports = new TransactionService();
