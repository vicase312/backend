const ApplicationError = require('./ApplicationError');

class EmailNotRegisteredError extends ApplicationError {
  constructor(User) {
    super(`${User.email} is not active, please check your email to activate your account.`);
  }
}

module.exports = EmailNotRegisteredError;
