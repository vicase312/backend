const ApplicationError = require('./ApplicationError');

class UnprocessableEntityError extends ApplicationError {
  constructor() {
    super(`Unprocessable Entity`);
  }
}

module.exports = UnprocessableEntityError;
