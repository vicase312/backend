'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Offers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Offers.belongsTo(models.Products, {
        foreignKey: 'product_id',
        as: 'product'
      });
      Offers.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user'
      });
      Offers.belongsTo(models.User, {
        foreignKey: 'seller_id',
        as: 'seller'
      });
    }
  }
  Offers.init(
    {
      user_id: DataTypes.INTEGER,
      seller_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      offer_price: DataTypes.INTEGER,
      status: DataTypes.INTEGER,
      deleted_at: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'Offers',
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at'
    }
  );
  return Offers;
};
