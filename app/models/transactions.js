'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Transaction.belongsTo(models.Products, {
        foreignKey: 'product_id',
        as: 'product'
      });
      Transaction.belongsTo(models.User, {
        foreignKey: 'buyer_id',
        as: 'buyer'
      });
      Transaction.belongsTo(models.User, {
        foreignKey: 'seller_id',
        as: 'seller'
      });
    }
  }
  Transaction.init(
    {
      invoice_id: DataTypes.STRING,
      buyer_id: DataTypes.INTEGER,
      buyer_name: DataTypes.STRING,
      buyer_address: DataTypes.STRING,
      buyer_phone: DataTypes.STRING,
      seller_id: DataTypes.INTEGER,
      seller_name: DataTypes.STRING,
      seller_address: DataTypes.STRING,
      seller_phone: DataTypes.STRING,
      product_id: DataTypes.STRING,
      offer_id: DataTypes.STRING,
      price: DataTypes.INTEGER,
      deleted_at: DataTypes.DATE
    },
    {
      sequelize,
      modelName: 'Transaction',
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at'
    }
  );
  return Transaction;
};
