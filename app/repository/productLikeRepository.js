const { ProductLike, Products, ProductImage } = require('../models');

class ProductLikeRepository {
  async getByUserAndProduct(user_id, product_id) {
    const like = await ProductLike.findOne({
      where: {
        user_id: user_id,
        product_id: product_id
      }
    });
    return like;
  }

  async add(user_id, product_id) {
    await ProductLike.create({
      user_id,
      product_id
    });
  }

  async delete(id) {
    await ProductLike.destroy({
      where: {
        id: id
      }
    });
  }

  async checkStatus(user_id, product_id) {
    const like = await ProductLike.findOne({
      where: {
        user_id,
        product_id
      }
    });
    return like;
  }

  async getByUser(user_id) {
    const like = await ProductLike.findAll({
      where: {
        user_id
      },
      include: [{ model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }],
      order: [['created_at', 'DESC']]
    });
    return like;
  }
}

module.exports = new ProductLikeRepository();
