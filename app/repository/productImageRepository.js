const { ProductImage } = require('../models');

class ProductImageRepository {
  add(product_id, url) {
    ProductImage.create({
      product_id,
      url
    });
  }

  async deleteAll(id) {
    await ProductImage.destroy({
      where: {
        product_id: id
      }
    });
  }

  async findByProduct(id) {
    const images = await ProductImage.findAll({
      where: {
        product_id: id
      }
    });

    return images;
  }
}

module.exports = new ProductImageRepository();
