const { Notification } = require('../models');

class NotificationRepository {
  async filterPagination({ where }, pagination) {
    const { page, page_size } = pagination;
    const notification = await Notification.findAll({
      where: where,
      offset: (page - 1) * page_size,
      limit: page_size,
      order: [['created_at', 'DESC']]
    });
    return notification;
  }

  async count({ query }) {
    const product = await Notification.count({
      where: query
    });
    return product;
  }

  async filterOne({ where }) {
    const notification = await Notification.findOne({
      where: where,
      order: [['created_at', 'DESC']]
    });
    return notification;
  }

  async create({ newData }) {
    const notification = await Notification.create(newData);
    return notification;
  }

  async updateRead(id) {
    const notification = await Notification.update(
      { is_read: true },
      {
        where: {
          id
        }
      }
    );
    return notification;
  }
}

module.exports = new NotificationRepository();
