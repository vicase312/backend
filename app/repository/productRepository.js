const { Products, Category, ProductImage, User, City } = require('../models');

class ProductRepository {
  async findAll({ query, page, page_size }) {
    const product = await Products.findAll({
      where: query,
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ],
      offset: (page - 1) * page_size,
      limit: page_size,
      order: [['created_at', 'DESC']]
    });
    return product;
  }

  async count({ query }) {
    const product = await Products.count({
      where: query
    });
    return product;
  }

  async delete(id) {
    const result = await Products.destroy({
      where: {
        id
      }
    });

    return result;
  }

  async edit(id, { name, price, category_id, description, status }) {
    const product = await Products.update(
      { name, price, category_id, description, status },
      {
        where: {
          id
        }
      }
    );
    return product;
  }

  async create(id, { name, price, category_id, description, status }, slug) {
    const product = await Products.create({
      user_id: id,
      name,
      price,
      status,
      category_id,
      description,
      slug
    });
    return product;
  }

  async getBySlug(slug) {
    const product = await Products.findOne({
      where: {
        slug: slug
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' },
        {
          model: User,
          as: 'user',
          attributes: { exclude: ['password'] },
          include: { model: City, as: 'city' }
        }
      ]
    });
    return product;
  }

  async getByProductId(id) {
    const product = await Products.findOne({
      where: {
        id
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ]
    });
    return product;
  }

  async getSoldByOwner(id) {
    const product = await Products.findAll({
      where: {
        user_id: id,
        status: 2
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ]
    });
    return product;
  }

  async getNotSoldByOwner(id) {
    const product = await Products.findAll({
      where: {
        user_id: id,
        status: [0, 1]
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ]
    });
    return product;
  }

  async getProductCountByOwner(id) {
    const product = await Products.count({
      where: {
        user_id: id
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ]
    });
    return product;
  }

  async getByOwnerAndId(user_id, id) {
    const product = await Products.findOne({
      where: {
        user_id: user_id,
        id: id
      },
      include: [
        { model: Category, as: 'category' },
        { model: ProductImage, as: 'images' }
      ]
    });
    return product;
  }
}

module.exports = new ProductRepository();
