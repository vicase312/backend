const bcrypt = require('bcryptjs');

const { User, City } = require('../models');
const { encryptPassword } = require('../libs/password');

class UserRepository {
  async createUser(userData, token) {
    const { name, email, password } = userData;

    const user = await User.create({
      name,
      email,
      status: 0,
      password: encryptPassword(password),
      token
    });

    return user;
  }

  async updateUser({ newData, id }) {
    const user = await User.update(newData, {
      where: {
        id
      }
    });
    return user;
  }

  async updateStatusUser(email) {
    const user = await User.update(
      {
        status: 1,
        token: null
      },
      { where: { email: email } }
    );

    return user;
  }

  async updateUserToken(randomToken, email) {
    const user = await User.update(
      {
        token: randomToken
      },
      { where: { email: email } }
    );

    return user;
  }

  async findByEmail(email) {
    const user = await User.findOne({
      where: {
        email
      }
    });
    return user;
  }

  async getUserWithoutPassword(id) {
    const user = await User.findOne({
      where: {
        id
      },
      attributes: {
        exclude: ['password', 'deleted_at', 'deleted_by']
      },
      include: [{ model: City, as: 'city' }]
    });
    return user;
  }
}

module.exports = new UserRepository();
