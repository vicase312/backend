const { Transaction, User, Products, ProductImage } = require('../models');

class TransactionRepository {
  getBySellerID = async (seller_id) => {
    const transaction = await Transaction.findAll({
      where: {
        seller_id
      },
      include: [
        { model: User, as: 'buyer' },
        { model: User, as: 'seller' },
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }
      ],
      order: [['created_at', 'DESC']]
    });

    return transaction;
  };

  getByBuyerID = async (buyer_id) => {
    const transaction = await Transaction.findAll({
      where: {
        buyer_id
      },
      include: [
        { model: User, as: 'buyer' },
        { model: User, as: 'seller' },
        { model: Products, as: 'product', include: { model: ProductImage, as: 'images' } }
      ],
      order: [['created_at', 'DESC']]
    });

    return transaction;
  };
}

module.exports = new TransactionRepository();
