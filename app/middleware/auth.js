const { decodeToken } = require('../libs/token');
const UserRepository = require('../repository/userRepository');
const { TokenInvalidError, TokenMustBeProvidedError } = require('../errors');

const checkAccessToken = async (req, res, next) => {
  const tokenBearer = req.headers.authorization;
  const token = tokenBearer?.split('Bearer ')[1];

  if (!token) {
    const err = new TokenMustBeProvidedError();
    return res.status(401).json(err);
  }

  try {
    const decoded = decodeToken(token);
    req.user = await UserRepository.getUserWithoutPassword(decoded.user.id);
    next();
  } catch (error) {
    const err = new TokenInvalidError();
    return res.status(400).json(err);
  }
};

module.exports = {
  checkAccessToken
};
