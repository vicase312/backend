'use strict';
const { Op } = require('sequelize');
const categories_name = ['Aksesoris', 'Kendaraan', 'Elektronik', 'Baju', 'Kesehatan'];

module.exports = {
  async up(queryInterface, Sequelize) {
    const timestamp = new Date();
    const categories = categories_name.map((category) => ({
      name: category,
      created_at: timestamp,
      updated_at: timestamp
    }));

    await queryInterface.bulkInsert('categories', categories, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete(
      'categories',
      {
        name: {
          [Op.in]: categories_name
        }
      },
      {}
    );
  }
};
