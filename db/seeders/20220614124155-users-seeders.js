'use strict';

const { Op } = require('sequelize');
const bcrypt = require('bcryptjs');

const names = ['Al', 'Rakha', 'Rayen', 'Rahul', 'Burhan'];

module.exports = {
  async up(queryInterface, Sequelize) {
    const password = '1234567';
    const encryptedPassword = bcrypt.hashSync(password, 10);
    const timestamp = new Date();

    const users = names.map((name) => ({
      name,
      email: `${name.toLowerCase()}@mailinator.com`,
      password: encryptedPassword,
      image: 'http://loremflickr.com/640/480',
      city_id: 1,
      address: 'jalan-jalan',
      phone: '081234567',
      status: 1,
      created_at: timestamp,
      updated_at: timestamp
    }));

    await queryInterface.bulkInsert('users', users, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete(
      'users',
      {
        name: {
          [Op.in]: names
        }
      },
      {}
    );
  }
};
