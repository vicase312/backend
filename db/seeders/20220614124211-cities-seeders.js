'use strict';
const { Op } = require('sequelize');
const cities_name = ['Bandung', 'Jakarta', 'Surabaya', 'Aceh', 'Pontianak'];

module.exports = {
  async up(queryInterface, Sequelize) {
    const timestamp = new Date();
    const cities = cities_name.map((city) => ({
      name: city,
      created_at: timestamp,
      updated_at: timestamp
    }));
    await queryInterface.bulkInsert('cities', cities, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete(
      'cities',
      {
        name: {
          [Op.in]: cities_name
        }
      },
      {}
    );
  }
};
