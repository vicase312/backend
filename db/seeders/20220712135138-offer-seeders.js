'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    const timestamp = new Date();
    return await queryInterface.bulkInsert('offers', [
      {
        user_id: 2,
        product_id: 16,
        offer_price: 3000000,
        status: 1,
        created_at: timestamp,
        updated_at: timestamp
      }
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
