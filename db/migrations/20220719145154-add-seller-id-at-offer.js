'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('offers', 'seller_id', { type: Sequelize.INTEGER });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
