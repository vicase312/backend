'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('products', 'slug', { type: Sequelize.STRING });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
