'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.changeColumn('products', 'description', {
      type: Sequelize.TEXT
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
