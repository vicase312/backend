'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'token', Sequelize.STRING);
  },

  async down(queryInterface, Sequelize) {}
};
