'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      buyer_id: {
        type: Sequelize.INTEGER
      },
      buyer_name: {
        type: Sequelize.STRING
      },
      buyer_address: {
        type: Sequelize.STRING
      },
      buyer_phone: {
        type: Sequelize.STRING
      },
      seller_id: {
        type: Sequelize.INTEGER
      },
      seller_name: {
        type: Sequelize.STRING
      },
      seller_address: {
        type: Sequelize.STRING
      },
      seller_phone: {
        type: Sequelize.STRING
      },
      product_id: {
        type: Sequelize.INTEGER
      },
      offer_id: {
        type: Sequelize.INTEGER
      },
      price: {
        type: Sequelize.INTEGER
      },
      deleted_at: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('transactions');
  }
};
